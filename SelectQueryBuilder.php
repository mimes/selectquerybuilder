class SelectQueryBuilder
{
    // property declaration
    private $start = 'select ';
    private $end = ";";
    private $whereArray = [];
    private $fromArray = [];
    private $selectArray = [];
    private $groupByArray = [];
    //todo: private $havingArray
    private $orderByArray = [];
    private $isDesc = false;
    private $limit = 99999999; //arbitrarily large number

    //for turning arrays of params into a comma seperated string
    //oof, maybe not efficient but we'll live 
    public static function paramsToString($array, $delim){
	    $result = implode(" {$delim} ", $array);
    	return $result;
    }

    // for adding conditions
    public function addWheres($whereArray) {
        foreach($whereArray as $where){
        	array_push($this->whereArray, $where);
        }
    }

    //for adding froms
    public function addFroms($fromArray){
    	foreach($fromArray as $from){
    		array_push($this->fromArray, $from);
    	}
    }

    //for adding groupbys
    public function addGroupBys($groupByArray){
    	foreach($groupByArray as $group){
    		array_push($this->groupByArray, $group);
    	}
    }

	//for adding orderbys
    public function addOrderBys($orderByArray){
    	foreach($orderByArray as $order){
    		array_push($this->orderByArray, $order);
    	}
    }

    //for adding select...things?
    public function addSelects($selectArray){
    	foreach($selectArray as $select){
    		array_push($this->selectArray, $select);
    	}
    }

    //for setting desc/asc
    public function setDescToTrue(){
    	$this->isDesc = true;
    }

    //set limit
    public function setLimit($limit){
    	$this->limit = $limit;
    }

    public function getQueryString(){
    	$output = "";
    	$output .= $this->start;

    	//add the select first
    	$output .= SelectQueryBuilder::paramsToString($this->selectArray, ",");
    	
    	//then the from
    	$output.= " from " . SelectQueryBuilder::paramsToString($this->fromArray, ",");
    	//then the where
    	if(sizeof($this->whereArray) > 0){
    		$output .= " where " . SelectQueryBuilder::paramsToString($this->whereArray, "and");
    	}
    	//then the group by
    	if(sizeof($this->groupByArray) > 0){
    		$output .=  " group by " . SelectQueryBuilder::paramsToString($this->groupByArray, ",");
    	}
		
    	//then the order
    	if(sizeof($this->orderByArray) > 0){
    		$output .= " order by " .SelectQueryBuilder::paramsToString($this->orderByArray, ",");
    	}
    	//then asc/desc
    	if($this->isDesc){
    		$output .= "desc";
    	}

    	$output .= $this->end;

    	return $output;
    }
}

